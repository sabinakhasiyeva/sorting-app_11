package com.example.sorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class InvalidArgument {
    private final String[] args;

    public InvalidArgument(String[] args) {
        this.args = args;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(new Object[][]{
                {new String[]{"abc", "fgh"}},
                {new String[]{"c", "q", "e"}},
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSortArray_whenInvalidArguments() {
        int[] intArgs = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                intArgs[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                // Handle the invalid argument case here if needed
                throw new IllegalArgumentException("Invalid argument: " + args[i]);
            }
        }

        SortingApp.sort(intArgs);
    }
}
