package com.example.sorting;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;



import java.util.Arrays;

public class SortingApp {
    private static final Logger logger = LogManager.getLogger(SortingApp.class);



    public static Object sort(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("No arguments provided");
        }
        int n = array.length;
        if (array.length > 10) {
            throw new IllegalArgumentException("There are more than 10 elements");
        }
        int[] numbers = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            try {
                numbers[i] = Integer.parseInt(String.valueOf(array[i]));
            } catch (NumberFormatException e) {
                System.out.println("Invalid argument: " + array[i]);

            }
        }

        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = 0; j < numbers.length - i - 1; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    // Swap numbers[j] and numbers[j + 1]
                    int temp = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = temp;
                }
            }
        }



            logger.info("Sorting numbers: {}", Arrays.toString(numbers));
        return numbers;
        }
    }


