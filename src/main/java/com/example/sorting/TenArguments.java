package com.example.sorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;


@RunWith(Parameterized.class)
public class TenArguments {
    private final int[] input;
    private final int[] expectedOutput;

    public TenArguments(int[] input, int[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(
                new Object[][]{
                        {new int[]{5, 3, 8, 2, 9, 3, 4, 1, 7, 6}, new int[]{1, 2, 3, 3, 4, 5, 6, 7, 8, 9}},
                        {new int[]{5, 9, 2, 3, 4, 2, 6, 8}, new int[]{2, 2, 3, 4, 5, 6, 8, 9}},
                        {new int[]{6, 3, 9, 4}, new int[]{3, 4, 6, 9}},
                }
        );
    }

    @Test
    public void TenArgumentsCase() {
        Object result = SortingApp.sort(input);
        assertArrayEquals(expectedOutput, (int[]) result);
    }
}
