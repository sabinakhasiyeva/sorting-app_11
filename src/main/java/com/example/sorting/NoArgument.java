package com.example.sorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class NoArgument {
    private final int[] input;
    private final int[] expectedOutput;

    public NoArgument(int[] input, int[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(
                new Object[][]{
                        {null, null},
                }
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNoArgumentCase() {
        SortingApp.sort(input);

    }
}
