package com.example.sorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;


@RunWith(Parameterized.class)
public class OneArgument {
    private final int[] input;
    private final int[] expectedOutput;

    public OneArgument(int[] input, int[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(
                new Object[][]{
                        {new int[]{1}, new int[]{1}},
                        {new int[]{6}, new int[]{6}},
                        {new int[]{10}, new int[]{10}},
                        // Add more test cases here...
                }
        );
    }

    @Test
    public void OneArgumentsCase() {
        SortingApp.sort(input);
        assertArrayEquals(expectedOutput, input);
    }
}
