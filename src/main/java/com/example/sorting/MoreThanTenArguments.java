package com.example.sorting;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class MoreThanTenArguments {
    private final int[] input;
    private final int[] expectedOutput;

    public MoreThanTenArguments(int[] input, int[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(
                new Object[][]{
                        {new int[]{6, 3, 8, 3, 10, 3, 4, 1, 7, 8, 5, 9, 5, 3, 6}, new int[]{5, 3, 8, 2, 10, 3, 4, 1, 7, 8, 5, 6}},
                        {new int[]{5, 3, 8, 2, 10, 3, 4, 1, 7, 8, 5, 6}, new int[]{5, 3, 8, 2, 10, 3, 4, 1, 7, 8, 5, 6}},
                        // Add more test cases here...
                }
        );
    }

    @Test(expected = IllegalArgumentException.class)
    public void MoreThanTenArgumentsCase() {
        SortingApp.sort(input);
    }
}
