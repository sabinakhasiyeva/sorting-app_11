package com.example.sorting;

import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import static org.junit.Assert.assertEquals;


public class SortingAppTest {

    @Test
    public void testTenArguments() {
        JUnitCore junit = new JUnitCore();
        Result result = junit.run(TenArguments.class);

        assertEquals(0, result.getFailureCount());
        assertEquals(3, result.getRunCount());
    }
    @Test
    public void testOneArgument() {
        JUnitCore junit = new JUnitCore();
        Result result = junit.run(OneArgument.class);

        assertEquals(0, result.getFailureCount());
        assertEquals(3, result.getRunCount());
    }
    @Test
    public void testNoArgument() {
        JUnitCore junit = new JUnitCore();
        Result result = junit.run(NoArgument.class);

        assertEquals(0, result.getFailureCount());
        assertEquals(1, result.getRunCount());
    }
    @Test
    public void testInvalidArgument() {
        JUnitCore junit = new JUnitCore();
        Result result = junit.run(InvalidArgument.class);

        assertEquals(0, result.getFailureCount());
        assertEquals(2, result.getRunCount());
    }
    @Test
    public void testMoreThanTenArguments() {
        JUnitCore junit = new JUnitCore();
        Result result = junit.run(MoreThanTenArguments.class);

        assertEquals(0, result.getFailureCount());
        assertEquals(2, result.getRunCount());
    }
}